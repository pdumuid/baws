__baws_launch_instance_complete()
{
  local cur prev opts
  COMPREPLY=()
  cur="${COMP_WORDS[COMP_CWORD]}"
  prev="${COMP_WORDS[COMP_CWORD-1]}"

  configFiles=`ls instance-config*.baws`
  withoutPrefix=${configFiles//instance-config-/ }
  opts=${withoutPrefix//.baws/ }

  COMPREPLY=($(compgen -W "${opts}" "${cur}"))

  return 0
}

complete -F __baws_launch_instance_complete baws-launch-instance
