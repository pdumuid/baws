#!/usr/bin/env python2

import argparse
import sys

try:
    import yaml
except ImportError:
    sys.stderr.write("Python package PyYAML not available\n")
    sys.exit(100)
         
def main():
    default_yaml = "valid: yaml"

    parser = argparse.ArgumentParser(description='Validate YAML data')
    parser.add_argument('-f', '--file', metavar='FILE', help='A file to analyse for YAML data')
    parser.add_argument('STDIN', type=str, help='run with - to take input from STDIN', nargs='?')
    options = parser.parse_args()
    
    if options.STDIN == '-':
        data = sys.stdin.readlines()
        yaml_data = "\n".join(data)
    else:
        f = open(options.file, 'r')
        yaml_data = f.read()

    try:
        objects = yaml.safe_load(yaml_data)
        print("Successfully validated YAML")
        return 0
    except Exception:
        sys.stderr.write("Data was not valid YAML\n")
        return 1

if __name__ == '__main__':
    sys.exit( main() )
