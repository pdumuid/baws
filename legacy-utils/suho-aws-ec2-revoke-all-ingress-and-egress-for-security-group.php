#!/usr/bin/env php
<?php

namespace SuHo\Toolkit\cli;
use Commando;
use Aws\Common\Aws;
use Aws\Ec2\Ec2Client;

require __DIR__.'/vendor/autoload.php';

$deployCommand = new Commando\Command();

$home = getenv("HOME");
$deployCommand->option('i')
    ->aka('ini-file')
    ->describedAs("The aws tool ini filename to use. [$home/.aws/config]")
    ->default("$home/.aws/credentials");

$deployCommand->option('r')
    ->aka('region')
    ->describedAs("The aws region. [value in $home/.aws/config]")
    ->default(null);

$deployCommand->option('g')
    ->aka("group-id")
    ->describedAs("The security group-id")
    ->default(null);

if ($deployCommand['help']) {
    exit(1);
}

if (!is_file($deployCommand['ini-file'])) {
    $deployCommand->error(new \Exception("The requested ini file does not exists incorrect!"));
    exit();
}

$inifileContents = parse_ini_file($deployCommand['ini-file'], true);

$awsAccessKeyId     = isset($inifileContents['default']['aws_access_key_id']) ? $inifileContents['default']['aws_access_key_id'] : null;
$awsSecretAccessKey = isset($inifileContents['default']['aws_secret_access_key']) ? $inifileContents['default']['aws_secret_access_key'] : null;

if (is_null($awsAccessKeyId) || is_null($awsSecretAccessKey)) {
    $deployCommand->error(new \Exception("Error reading the ini file, ".$deployCommand['ini-file']));
    exit(1);
}

$cliAWSRegion = $deployCommand['region'];
$awsRegion = !empty($cliAWSRegion) ? $cliAWSRegion : (isset($inifileContents['default']['region']) ? $inifileContents['default']['region'] : null);

$ec2Client = Ec2Client::factory(array('key' => $awsAccessKeyId, 'secret' => $awsSecretAccessKey, 'region' => $awsRegion));

$securityGroupId = $deployCommand['group-id'];
if (empty($securityGroupId)) {
    $deployCommand->error(new \Exception("The security group must be specified"));
    exit(1);
}
 
try {
    $groupInfo = $ec2Client->describeSecurityGroups(array("GroupIds" => array($securityGroupId),));
    if (!empty($groupInfo['SecurityGroups'][0]['IpPermissions'])) {
        $revokeArray = array(
            'GroupId'    => $securityGroupId,
            'DryRun'     => false,
            'IpPermissions' => $groupInfo['SecurityGroups'][0]['IpPermissions']
        );

        $r1 = $ec2Client->revokeSecurityGroupIngress($revokeArray);
    }

    if (!empty($groupInfo['SecurityGroups'][0]['IpPermissionsEgress'])) {
        $revokeArrayEgress = array(
            'GroupId'       => $securityGroupId,
            'DryRun'        => false,
            'IpPermissions' => $groupInfo['SecurityGroups'][0]['IpPermissionsEgress']
        );
        $r2 = $ec2Client->revokeSecurityGroupEgress($revokeArrayEgress);
    }
} catch (\Exception $e) {
    $deployCommand->error($e);
}


