#
# Function definitions for launch-instance
#

# Colour codes
C_FAILED=$(echo -e "\033[0;31m")
C_WARN=$(echo   -e "\033[0;33m")
C_INFO=$(echo   -e "\033[1;37m")
C_DONE=$(echo   -e "\033[0;32m")
C_LOG=$(echo    -e "\033[0;37m")
C_END=$(echo    -e "\033[0m ")

################################################################################
#
# Display key pairs that are available in this region
# Needs
# - $REGION
#
# Sets
# - $KEY_PAIR
#
function selectKeyPair() {

    select key in `aws ec2 describe-key-pairs --region $REGION | jq -r ".KeyPairs[].KeyName"`; do
        KEY_PAIR=$key;
        break;
    done
}

################################################################################
#
# Remove any temporary files created during the launch
#
# When creating temporary files they should be added to the TEMP_FILES array
#
# Uses
# - $TEMP_FILES
#
function cleanupTemporaryFiles() {
    if [ ${#TEMP_FILES[@]} -gt 0 ]; then
        rm ${TEMP_FILES}
    fi
}

################################################################################
#
# Monitor the configuration progress of the launched instance using ec2gcons
#
# This method is more secure, but fails when cloud-config scripts are very long
#
# Requires
# - $REGION
# - $INSTANCE_ID
#
function monitorConfigurationViaGCons() {
    log "Configuration running - monitoring configuration via gcons"
    echo "For more info see 'tail -f /tmp/ec2.$INSTANCE_ID.output.log'"

    echo "Starting" > /tmp/ec2.$INSTANCE_ID.output.log

    sleep 30s

    FINISHED=`tail /tmp/ec2.$INSTANCE_ID.output.log | grep "END SSH HOST"`

    while [ -z "$FINISHED" ]; do
        log_progress
        sleep 15s
        aws ec2 get-console-output --instance-id $INSTANCE_ID --region $REGION > /tmp/ec2.$INSTANCE_ID.output.log

        FINISHED=`tail /tmp/ec2.$INSTANCE_ID.output.log | grep "cloud-init boot finished"`
    done

    log_done
}

################################################################################
#
# Monitor the configuration progress of the launched instance using ssh
#
# This method is more reliable and slightly quicker than via ec2gcons, but is
# vulnerable to man-in-the-middle attacks
#
# Requires
# - $PUBLIC_DNS or $IP_ADDRESS
# - $INSTANCE_ID
#
function monitorConfigurationViaSSH() {
    HOST=${IP_ADDRESS:=$PUBLIC_DNS};

    # --- This is the bit that is vulnerable, it assumes the key is to be trusted
    log "Getting public key"
    getHostPublicKey ${HOST}  > /tmp/${INSTANCE_ID}.keys
    log_done

    log "Adding ${HOST} to known hosts "
    if [ $? -ne 0 ]; then log_failed; errorAndExit "Failed to get keyscan" $ERROR_SECURITY; fi
    ssh-keygen -R "${HOST}" &> /dev/null
    ssh-keygen -H -f /tmp/${INSTANCE_ID}.keys &> /dev/null
    if [ $? -ne 0 ]; then log_failed; errorAndExit "Failed to add host" $ERROR_SECURITY; fi
    cat /tmp/${INSTANCE_ID}.keys >> ~/.ssh/known_hosts
    rm  /tmp/${INSTANCE_ID}.*

    log_done
    #------------------------

    log "Configuration running"

    until `scp ubuntu@$HOST:/tmp/cloud_init_completed /dev/null >&/dev/null`; do
        log_progress
        sleep 15s
    done
    log_done

    log "Copying final log to /tmp/ec2.$INSTANCE_ID.output.log"
    scp ubuntu@$HOST:/tmp/final.stdout /tmp/ec2.$INSTANCE_ID.output.log >&/dev/null
    log_done
}

function getHostPublicKey() {
    until [ ! -z "${PUBLIC_KEY}" ]; do
        PUBLIC_KEY=$(ssh-keyscan $1  2> /dev/null)
        sleep 3
    done

    echo $PUBLIC_KEY
}


function errorAndExit() {
    error "${1}"
    showTotalExecutionTime
    exit $2
}

function notice() {
    echo "$1"
}

# Inform the user of the current actions (e.g. "Launch and tag instances")
function inform() {
    if [ -z "$QUIET" ]; then
        echo "${C_INFO}${1}${C_END}"
    fi
}

# Same level as log, but without the option of adding _done
function info() {
    if [ -z "$QUIET" ]; then
        log "${1}${C_END}"
        echo ""
    fi
}

# Log the start of an action
function log() {
    if [ -z "$QUIET" ]; then
        echo -n $C_LOG
        printf "  %-100s" "$1"
        echo -n $C_END
    fi
    LOG_TIME_START=`perl -e 'use Time::HiRes; $s= Time::HiRes::gettimeofday(); print $s;'`
}

function log_progress() {
    echo -n "."
}

# Log the completion of an action
function log_done() {
    LOG_TIME_END=`perl -e 'use Time::HiRes; $s= Time::HiRes::gettimeofday(); print $s;'`
    LOG_TIME_DURATION=$(printf "%0.2f" `perl -e "print $LOG_TIME_END - $LOG_TIME_START"`)
    if [ -z "$QUIET" ]; then
        echo "${C_DONE}[done]${C_END} (${LOG_TIME_DURATION})"
    fi
}

function log_skipped() {
    if [ -z "$QUIET" ]; then
        echo "${C_WARN}[skipped]${C_END}"
    fi

    if [ ! -z "$1" ]; then
        notice "$1"
    fi
}

function log_failed() {
    if [ -z "$QUIET" ]; then
        echo "${C_FAILED}[failed]${C_END}"
    fi
}

# Display an error message to the user
function error() {
    echo "${C_FAILED}[ERROR]${C_END} $1" 2>&1
}

function showTotalExecutionTime() {
    SCRIPT_TIME_END=`perl -e 'use Time::HiRes; $s= Time::HiRes::gettimeofday(); print $s;'`
    SCRIPT_DURATION=$(printf "%0.2f" `perl -e "print $SCRIPT_TIME_END - $SCRIPT_TIME_START"`)
    echo  -e "\nTotal execution time: ${SCRIPT_DURATION} seconds"
}
