#
# Functions for general aws functions
#

function baws_get_ubuntu_ami() {
    NAME_PREFIX=$1
    if [ -z "$NAME_PREFIX" ]; then
        echo "You must specify a name prefix, e.g. ubuntu/images/hvm/ubuntu-trusty-14.04-amd64-server"
        exit 1
    fi
    if [ -z "$AWS_REGION" ]; then echo "AWS_REGION must be specified"; exit 1 ; fi
    USER_CANONICAL=099720109477

    # Perform our own get-ami since we need hvm                                                                                                              
    TEMP_FILENAME=`mktemp /tmp/baws_getUbuntuAMI_XXXX`

    aws ec2 describe-images --owner $USER_CANONICAL --region $AWS_REGION --filter "Name=name,Values=$NAME_PREFIX*" > $TEMP_FILENAME || return 1
    
    INSTANCE_AMI=`cat $TEMP_FILENAME | jq -r '.Images | sort_by(.Name) | .[].ImageId' | tail -n 1`
    
    unlink $TEMP_FILENAME
    echo $INSTANCE_AMI
}

function baws_install_key_pair() {
    KEY_NAME=$1
    KEY_FILENAME=$2
    
    if [ -z "$AWS_REGION" ];    then echo "ERROR: AWS_REGION not specified";          return 1; fi
    if [ -z "$KEY_NAME" ];      then echo "ERROR: ARG1 - key-name not specified";     return 1; fi
    if [ -z "$KEY_FILENAME" ];  then echo "ERROR: ARG2 - key-filename not specified"; return 1; fi

    KP_COUNT=`aws ec2 describe-key-pairs --region $AWS_REGION --filter "Name=key-name,Values=$1" | jq ".KeyPairs | length"`
    echo -n "Installing key pair, $KEY_NAME using $KEY_FILENAME"

    if [ "$KP_COUNT" = "0" ]; then
	aws ec2 import-key-pair \
	    --region $AWS_REGION \
	    --key-name "$KEY_NAME" \
	    --public-key-material file://$KEY_FILENAME > /dev/null || return 1
	echo " done"
    else
	echo " skippedAlreadyThere"
    fi
}

function baws_ask_for_region() {
    echo -e "\n\nPlease select a region:"
    echo "  1 north-virginia   (us-east-1)"
    echo "  2 north-california (us-west-1)"
    echo "  3 oregon           (us-west-2)"
    echo "  4 ireland          (eu-west-1)"
    echo "  5 singapore        (ap-southeast-1)"
    echo "  6 tokyo            (ap-northeast-1)"
    echo "  7 sydney           (ap-southeast-2)"
    echo "  8 sau-paulo        (sa-east-1)"
    echo "  9 frankfurt        (eu-central-1)"
    echo ""
    echo " (See src/docs/Setting-up-new-vpc.txt for help with setting up a new region)"
    echo ""
    echo -n "What region? "
    read REGION_KEY

    if   [ "$REGION_KEY" == "1" ]; then AWS_REGION="us-east-1";
    elif [ "$REGION_KEY" == "2" ]; then AWS_REGION="us-west-1";
    elif [ "$REGION_KEY" == "3" ]; then AWS_REGION="us-west-2";
    elif [ "$REGION_KEY" == "4" ]; then AWS_REGION="eu-west-1";
    elif [ "$REGION_KEY" == "5" ]; then AWS_REGION="ap-southeast-1";
    elif [ "$REGION_KEY" == "6" ]; then AWS_REGION="ap-northeast-1";
    elif [ "$REGION_KEY" == "7" ]; then AWS_REGION="ap-southeast-2";
    elif [ "$REGION_KEY" == "8" ]; then AWS_REGION="sa-east-1";
    elif [ "$REGION_KEY" == "9" ]; then AWS_REGION="eu-central-1";
    else
        echo "Sorry, you must specify a region."
        exit 1
    fi
    echo $AWS_REGION
}

function baws_aws_humanName_for_region() {
    if   [ "$1" == "us-east-1" ];      then  AWS_REGION_HUMAN_NAME='north-virginia';
    elif [ "$1" == "us-west-1" ];      then  AWS_REGION_HUMAN_NAME='north-california';
    elif [ "$1" == "us-west-2" ];      then  AWS_REGION_HUMAN_NAME='oregon';
    elif [ "$1" == "eu-west-1" ];      then  AWS_REGION_HUMAN_NAME='ireland';
    elif [ "$1" == "ap-southeast-1" ]; then  AWS_REGION_HUMAN_NAME='singapore';
    elif [ "$1" == "ap-northeast-1" ]; then  AWS_REGION_HUMAN_NAME='tokyo';
    elif [ "$1" == "ap-southeast-2" ]; then  AWS_REGION_HUMAN_NAME='sydney';
    elif [ "$1" == "sa-east-1" ];      then  AWS_REGION_HUMAN_NAME='sau-paulo';
    elif [ "$1" == "eu-central-1" ];   then  AWS_REGION_HUMAN_NAME='frankfurt';
    else
        echo "Sorry, you must specify a known region."
        exit 1
    fi
    echo $AWS_REGION_HUMAN_NAME
}


function baws_get_default_vpc_id() {
    if [ -z "$AWS_REGION" ]; then  echo "AWS_REGION is not define"; return 1; fi
    echo `aws ec2 describe-vpcs --region $AWS_REGION --filter "Name=is-default,Values=true" | jq -r ".Vpcs[].VpcId"` || return 1
}


function baws_get_vpc_id() {
    if [ -z "$AWS_REGION" ]; then  echo "AWS_REGION is not define"; return 1; fi
    if [ -z "$1"          ]; then  echo "Arg 1 - VPC name not set"   ; return 1; fi

    echo `aws ec2 describe-vpcs --region $AWS_REGION --filter "Name=tag:Name,Values=$1" | jq -r ".Vpcs[].VpcId"` || return 1
}

# USAGE: VPC_ID=vpc-79448f1c AWS_REGION=ap-southeast-1 baws_security_groups_declarer /tmp/x && source /tmp/x || exit 1
function baws_security_groups_declarer() {
    if [ -z "$AWS_REGION" ]; then  echo "AWS_REGION is not define"; return 1; fi
    if [ -z "$VPC_ID"     ]; then  echo "VPC_ID is not defined"   ; return 1; fi
    if [ -z "$1"          ]; then  echo "Arg 1 filename not defined"   ; return 1; fi

    F_OUT2=`mktemp /tmp/suho_dtso_getSG_XXX` || return 1

    aws ec2 describe-security-groups --region $AWS_REGION --filter "Name=vpc-id,Values=$VPC_ID" > $F_OUT2 || return 1
    declare -A SECURITY_GROUPS
    for NAME_AND_ID in `cat $F_OUT2 | jq -r '.SecurityGroups[] | .GroupName + "/" + .GroupId'`; do
        GROUP_NAME=`echo $NAME_AND_ID | sed 's=/.*$=='`
        GROUP_ID=`echo $NAME_AND_ID | sed 's=^.*/=='`
        SECURITY_GROUPS[$GROUP_NAME]="$GROUP_ID"
    done
    declare -p SECURITY_GROUPS > $1 || exit 1
    unlink $F_OUT2
}

function baws_as_create_launchconfig_and_create_or_update_autoscalegroup() {
    if [ -z "$AWS_REGION"                  ]; then echo "AWS_REGION not set"                  ; return 1; fi
    if [ -z "$LAUNCH_CONFIG_NAME"          ]; then echo "LAUNCH_CONFIG_NAME not set"          ; return 1; fi
    if [ -z "$AS_INSTANCE_TYPE"            ]; then echo "AS_INSTANCE_TYPE not set"            ; return 1; fi
    if [ -z "$AMI_ID"                      ]; then echo "AMI_ID not set"                      ; return 1; fi
    if [ -z "$AS_GROUP_NAME"               ]; then echo "AS_GROUP_NAME not set"               ; return 1; fi
    if [  ${#AS_INSTANCES_TAGS[@]} -eq 0   ]; then echo "AS_INSTANCES_TAGS not set"           ; return 1; fi
    if [ -z "$AS_GROUP_CREATION_MAX_SIZE"  ]; then echo "AS_GROUP_CREATION_MAX_SIZE not set"  ; return 1; fi
    if [ -z "$AS_GROUP_CREATION_SUBNETS"   ]; then echo "AS_GROUP_CREATION_SUBNETS not set"   ; return 1; fi
    if [ -z "$AWS_AMI_KEYPAIR_NAME"        ]; then echo "AWS_AMI_KEYPAIR_NAME not set"        ; return 1; fi

    # --------------------------------------------
    echo "AutoScale/$AS_GROUP_NAME 1. - Creating launch configuration, $LAUNCH_CONFIG_NAME"
    # --------------------------------------------
    unset ARGS
    declare -A ARGS
    if [ ! -z "$AS_LC_FULL_DATA_FILENAME" ]; then
        ARGS[1]="--user-data"
        ARGS[2]="file://$AS_LC_FULL_DATA_FILENAME"
    fi

    ARG_I=3
    if [ ${#AS_INSTANCES_SECURITY_GROUPS[@]} -gt 0 ]; then
        ARGS[$ARG_I]="--security-groups"
        let ARG_I=ARG_I+1
        for i in "${!AS_INSTANCES_SECURITY_GROUPS[@]}"; do
            ARGS[$ARG_I]=${AS_INSTANCES_SECURITY_GROUPS[$i]}
            let ARG_I=ARG_I+1
        done
    fi

    if [ ! -z "$AS_LC_IMA_INSTANCE_PROFILE" ]; then
        ARGS[$ARG_I]="--iam-instance-profile"
        let ARG_I=ARG_I+1
        ARGS[$ARG_I]="$AS_LC_IMA_INSTANCE_PROFILE"
        let ARG_I=ARG_I+1
    fi

    aws autoscaling create-launch-configuration \
        --launch-configuration-name $LAUNCH_CONFIG_NAME \
        --region $AWS_REGION \
        --instance-type $AS_INSTANCE_TYPE \
        --image-id $AMI_ID \
        ${ARGS[@]} \
        --key-name $AWS_AMI_KEYPAIR_NAME || return 1

    AS_G_COUNT=`baws_as_group_count $AS_GROUP_NAME` || return 1
    if [ "$AS_G_COUNT" == "1" ]; then
        # --------------------------------------------
        echo "AutoScale/$AS_GROUP_NAME 2. - UPDATE the auto-scaling group, $AS_GROUP_NAME"
        # --------------------------------------------
        aws autoscaling update-auto-scaling-group \
            --region $AWS_REGION \
            --auto-scaling-group-name $AS_GROUP_NAME \
            --launch-configuration-name $LAUNCH_CONFIG_NAME \
            || return 1
    else
        # --------------------------------------------
        echo "AutoScale/$AS_GROUP_NAME 2. - CREATE the auto-scaling group, $AS_GROUP_NAME"
        # --------------------------------------------
        unset ARGS
        declare -A ARGS
        ARG_I=1
        for i in "${!AS_INSTANCES_TAGS[@]}"; do
            ARGS[$ARG_I]="ResourceId=$AS_GROUP_NAME,ResourceType=auto-scaling-group,Key=$i,Value=${AS_INSTANCES_TAGS[$i]},PropagateAtLaunch=true"
            let ARG_I=ARG_I+1
        done

        if [[ -v AS_LOAD_BALANCER_NAMES ]]; then
            ARGS[$ARG_I]="--load-balancer-names"
            let ARG_I=ARG_I+1
            for i in "${!AS_LOAD_BALANCER_NAMES[@]}"; do
                ARGS[$ARG_I]=${AS_LOAD_BALANCER_NAMES[$i]}
                let ARG_I=ARG_I+1
            done
        fi

        if [ ! "$AS_HEALTH_CHECK_TYPE" == "ELB" ]; then
            AS_HEALTH_CHECK_TYPE=EC2
        fi

        if [ ! -z "$AS_HEALTH_GRACE_PERIOD"  ]; then
            ARGS[$ARG_I]="--health-check-grace-period"
            let ARG_I=ARG_I+1
            ARGS[$ARG_I]=$AS_HEALTH_GRACE_PERIOD
        fi

        aws autoscaling create-auto-scaling-group \
            --region $AWS_REGION \
            --auto-scaling-group-name $AS_GROUP_NAME \
            --launch-configuration-name $LAUNCH_CONFIG_NAME \
            --max-size $AS_GROUP_CREATION_MAX_SIZE \
            --min-size 0 \
            --health-check-type $AS_HEALTH_CHECK_TYPE \
            --vpc-zone-identifier ${AS_GROUP_CREATION_SUBNETS} \
            --tags "${ARGS[@]}" \
            || return 1
    fi
}

function baws_aws_as_create_policies() {
    if [ -z "$AWS_REGION"                 ]; then echo "AWS_REGION not set"         ; return 1; fi
    if [ -z "$AS_GROUP_NAME"              ]; then echo "AS_GROUP_NAME not set"      ; return 1; fi
    if [  ${#AS_POLICY_SETTINGS[@]} -eq 0 ]; then echo "AS_POLICY_SETTINGS not set" ; return 1; fi

    echo -n "Creating policies: "
    
    for i in "${!AS_POLICY_SETTINGS[@]}"; do
        NAME=$i
        ADJUSTMENT_TYPE=`echo ${AS_POLICY_SETTINGS[$i]} | sed 's=/.*$=='`
        ADJUSTMENT_AMOUNT=`echo ${AS_POLICY_SETTINGS[$i]} | sed 's=^.*/=='`

        echo -n "$NAME.."
        aws autoscaling put-scaling-policy \
            --region $AWS_REGION \
            --auto-scaling-group $AS_GROUP_NAME \
            --policy-name $NAME \
            --adjustment-type "$ADJUSTMENT_TYPE" \
            --scaling-adjustment "$ADJUSTMENT_AMOUNT" > /dev/null || return 1
    done
    echo 
}

function baws_as_group_count() {
    if [ -z "$AWS_REGION"              ]; then echo "AWS_REGION                  not set"; exit 1; fi
    if [ -z "$1"                       ]; then echo "ARG1 - Autoscale Group Name not set"; exit 1; fi

    COUNT=`aws autoscaling describe-auto-scaling-groups \
        --region $AWS_REGION \
        --auto-scaling-group-names $1 | jq  '.AutoScalingGroups | length'` || exit 1
    echo $COUNT
}


