prefix	:= /usr/

install:
	test -d $(prefix)            || mkdir -p $(prefix)
	test -d $(prefix)/bin        || mkdir $(prefix)/bin
	test -d $(prefix)/share/baws || mkdir -p $(prefix)/share/baws
	install -m 0755 baws-launch-instance               $(prefix)/share/baws/
	ln -s $(prefix)/share/baws/baws-launch-instance    $(prefix)/bin/baws-launch-instance
	install -m 0644 _baws-functions.sh                 $(prefix)/share/baws/
	install -m 0644 _baws_launch-instance-functions.sh $(prefix)/share/baws/
	install -m 0644 baws-launch-instance.1             $(prefix)/share/baws/
	install -d                                         $(prefix)/share/baws/examples/
	install -m 0644 examples/*                         $(prefix)/share/baws/examples/

.PHONY: install
