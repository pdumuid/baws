#!/bin/bash

cat >> /root/.ssh/config <<EOF
host *
     IdentityFile /root/.ssh/git_clone_credentials.id_rsa
EOF

cat >> /root/.ssh/known_hosts <<EOF
EOF

cat > /root/.ssh/git_clone_credentials.id_rsa <<EOF
-----BEGIN RSA PRIVATE KEY-----
Contents of a RSA Private Key here
-----END RSA PRIVATE KEY-----
EOF

chmod 0600 /root/.ssh/git_clone_credentials.id_rsa
