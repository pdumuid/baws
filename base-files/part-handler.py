#part-handler
# vi: syntax=python ts=4
import subprocess
import os

def list_types():
    # return a list of mime-types that are handled by this module
    return(["text/plain", "text/x-httpd-php"])

def handle_part(data,ctype,filename,payload):
    # data: the cloudinit object
    # ctype: '__begin__', '__end__', or the specific mime-type of the part
    # filename: the filename for the part, or dynamically generated part if
    #           no filename is given attribute is present
    # payload: the content of the part (empty for begin or end)
    if ctype == "__begin__":
       print "[JOB-HANDLER] Start..."
       return
    if ctype == "__end__":
       print "[JOB-HANDLER] End"
       return

    print "[JOB-HANDLER] Received ctype=%s filename=%s" % (ctype,filename)
    print "  Saving file to /run/shm/cloud-init/",filename

    if not os.path.isdir('/run/shm/cloud-init'): 
        os.mkdir('/run/shm/cloud-init')

    try:
        f = open('/run/shm/cloud-init/'+filename, 'w')
        f.write(payload)
        f.close()
    except:
        print "Unexpected error:", sys.exc_info()[0]
    raise

    return
